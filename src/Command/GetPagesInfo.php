<?php

declare(strict_types=1);

namespace ContextualCode\Crawler\Command;

use ContextualCode\Crawler\Entity\Page;
use ContextualCode\Crawler\Repository\PageRepository;
use ContextualCode\Crawler\Service\Messages;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GetPagesInfo extends Command
{
    protected static $defaultName = 'crawler:get-pages-info';

    private const ARGUMENT_IDENTIFIER = 'identifier';
    private const OPTION_LIMIT = 'limit';
    private const OPTION_OFFSET = 'offset';

    /** @var Messages */
    private $messages;

    /** @var PageRepository */
    private $pageRepository;

    public function __construct(Messages $messages, PageRepository $pageRepository)
    {
        $this->messages = $messages;
        $this->pageRepository = $pageRepository;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument(
                self::ARGUMENT_IDENTIFIER,
                InputArgument::REQUIRED,
                $this->messages->get('command_argument_identifier')
            )
            ->addOption(
                self::OPTION_LIMIT,
                null,
                InputOption::VALUE_OPTIONAL,
                $this->messages->get('command_option_limit'),
            )
            ->addOption(
                self::OPTION_OFFSET,
                null,
                InputOption::VALUE_OPTIONAL,
                $this->messages->get('command_option_offset')
            )
            ->setDescription($this->messages->get('command_mime_types_description'));
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $identifier = $input->getArgument(self::ARGUMENT_IDENTIFIER);
        $limit = $input->getOption(self::OPTION_LIMIT);
        $offset = $input->getOption(self::OPTION_OFFSET);
        $pages = $this->pageRepository->findBy([
            'identifier' => $identifier,
        ], null, $limit, $offset);
        $data = [];
        /** @var Page $page */
        foreach ($pages as $page) {
            $data[] = [
                $page->isValid(),
                $page->isRedirect(),
                $page->getContentType(),
                $page->getStatusCode(),
                $page->getUrl(),
            ];
        }
        $headers = [
            $this->messages->get('page_valid'),
            $this->messages->get('page_redirect'),
            $this->messages->get('page_type'),
            $this->messages->get('page_status_code'),
            $this->messages->get('page_url'),
        ];
        $io->table($headers, $data);

        return 0;
    }
}
