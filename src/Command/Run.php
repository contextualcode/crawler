<?php

declare(strict_types=1);

namespace ContextualCode\Crawler\Command;

use ContextualCode\Crawler\Service\Crawler;
use ContextualCode\Crawler\Service\Messages;
use InvalidArgumentException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class Run extends Command
{
    protected static $defaultName = 'crawler:run';

    private const ARGUMENT_IDENTIFIER = 'identifier';
    private const OPTION_PURGE = 'purge';
    private const OPTION_RESUME = 'resume';

    /** @var Messages */
    private $messages;

    /** @var Crawler */
    private $crawler;

    public function __construct(Messages $messages, Crawler $crawler)
    {
        $this->messages = $messages;
        $this->crawler = $crawler;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument(
                self::ARGUMENT_IDENTIFIER,
                InputArgument::REQUIRED,
                $this->messages->get('command_argument_identifier')
            )
            ->addOption(
                self::OPTION_PURGE,
                null,
                InputOption::VALUE_NONE,
                $this->messages->get('command_option_purge')
            )
            ->addOption(
                self::OPTION_RESUME,
                null,
                InputOption::VALUE_NONE,
                $this->messages->get('command_option_resume')
            )
            ->setDescription($this->messages->get('command_run_description'));
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $identifier = $input->getArgument(self::ARGUMENT_IDENTIFIER);
        try {
            $this->crawler->setHandler($identifier);
        } catch (InvalidArgumentException $e) {
            $io->error($e->getMessage());

            return 1;
        }

        $progressBar = $this->getProgressBar($io, $this->crawler->getHomePageUrl());
        $this->crawler->setProgressBar($progressBar);

        if (!$input->getOption(self::OPTION_RESUME)) {
            $this->crawler->reset($input->getOption(self::OPTION_PURGE));
        }

        $message = $this->messages->get('command_live_logs', [$this->crawler->getLogFilepath()]);
        $io->block($message, null, 'fg=black;bg=yellow', ' ', true);

        $io->title($this->messages->get('command_run_title'));
        $progressBar->start(1);

        $info = $this->crawler->run();

        $progressBar->finish();
        $io->newLine(2);

        $message = $this->messages->get('command_run_success', $info);
        $io->block($message, null, 'fg=black;bg=green', ' ', true);

        return 0;
    }

    protected function getProgressBar(SymfonyStyle $io, string $url = ''): ProgressBar
    {
        ProgressBar::setFormatDefinition('custom', $this->getProgressBarFormat());

        $progressBar = $io->createProgressBar();
        $progressBar->setFormat('custom');
        $progressBar->setMessage($url, 'url');
        $progressBar->setMessage('', 'referer');

        return $progressBar;
    }

    protected function getProgressBarFormat(): string
    {
        return '  Url: %url%' . "\n\r"
            . '  Referer: %referer%' . "\n\r\n\r"
            . '%current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%';
    }
}
