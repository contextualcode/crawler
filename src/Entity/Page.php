<?php

declare(strict_types=1);

namespace ContextualCode\Crawler\Entity;

use Doctrine\ORM\Mapping as ORM;
use DOMDocument;
use DOMXPath;

/**
 * @ORM\Entity(repositoryClass="ContextualCode\Crawler\Repository\PageRepository")
 * @ORM\Table(name="cc_content_import_pages",indexes={@ORM\Index(name="valid_idx", columns={"is_valid"}), @ORM\Index(name="processed_idx", columns={"is_processed"})})
 */
class Page
{
    /**
     * @ORM\Column(type="string", length=32)
     * @ORM\Id
     */
    private $identifier;

    /**
     * @ORM\Column(type="string", length=736)
     * @ORM\Id
     */
    private $url;

    /** @ORM\Column(type="string", length=736, nullable=true) */
    private $referer;

    /** @ORM\Column(type="string", nullable=true) */
    private $refererPath;

    /** @ORM\Column(type="string", nullable=true) */
    private $contentType;

    /** @ORM\Column(type="smallint", nullable=true, options={"default": 200}) */
    private $statusCode = 200;

    /** @ORM\Column(type="string", nullable=true) */
    private $errorMessage;

    /** @ORM\Column(type="json", nullable=true) */
    private $headers;

    /** @ORM\Column(type="text", nullable=true) */
    private $response;

    /** @ORM\Column(type="string", length=736, nullable=true) */
    private $redirectUrl;

    /** @ORM\Column(type="boolean", nullable=true, options={"default": false}) */
    private $isValid = false;

    /** @ORM\Column(type="boolean", nullable=true, options={"default": false}) */
    private $isProcessed = false;

    /** @ORM\Column(type="boolean", nullable=true, options={"default": false}) */
    private $isRedirect = false;

    /** @ORM\Column(type="integer", options={"default": 0}) */
    private $pageOrder = 0;

    public function __construct(string $identifier, string $url)
    {
        $this->identifier = $identifier;
        $this->url = $url;
    }

    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function getReferer(): ?string
    {
        return $this->referer;
    }

    public function getRefererPath(): ?string
    {
        return $this->refererPath;
    }

    public function getContentType(): ?string
    {
        return $this->contentType;
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getErrorMessage(): ?string
    {
        return $this->errorMessage;
    }

    public function getRedirectUrl(): ?string
    {
        return $this->redirectUrl;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function getResponse(): ?string
    {
        return $this->response;
    }

    public function isValid(): bool
    {
        return $this->isValid;
    }

    public function isProcessed(): bool
    {
        return $this->isProcessed;
    }

    public function isRedirect(): bool
    {
        return $this->isRedirect;
    }

    public function getPageOrder(): int
    {
        return $this->pageOrder;
    }

    public function setIdentifier(string $identifier): void
    {
        $this->identifier = $identifier;
    }

    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    public function setReferer(?string $referer): void
    {
        $this->referer = $referer;
    }

    public function setRefererPath(?string $refererPath): void
    {
        $this->refererPath = $refererPath;
    }

    public function setContentType(string $contentType): void
    {
        // Remove the charset for HTML content type ("text/html; charset=UTF-8" => "text/html")
        if (strpos($contentType, ';') !== false) {
            $tmp = explode(';', $contentType);
            $contentType = $tmp[0];
        }

        $this->contentType = $contentType;
    }

    public function setStatusCode(int $statusCode): void
    {
        $this->statusCode = $statusCode;
    }

    public function setErrorMessage(?string $errorMessage): void
    {
        $this->errorMessage = $errorMessage;
    }

    public function setRedirectUrl(?string $url): void
    {
        $this->setRedirect(true);

        $this->redirectUrl = $url;
    }

    public function setHeaders(array $headers): void
    {
        $this->headers = $headers;
    }

    public function setResponse(?string $response): void
    {
        // Remove the characters which can not be stored in the DB
        $response = mb_convert_encoding($response, 'UTF-8', 'UTF-8');
        $response = preg_replace('/[^[:print:]\n]/u', '', $response);

        $this->response = $response;
    }

    public function setValid(bool $isValid): void
    {
        $this->isValid = $isValid;
    }

    public function setProcessed(bool $isProcessed): void
    {
        $this->isProcessed = $isProcessed;
    }

    public function setRedirect(bool $isRedirect): void
    {
        $this->isRedirect = $isRedirect;
    }

    public function setPageOrder(int $order): void
    {
        $this->pageOrder = $order;
    }

    public function getResponseXPath(): DOMXPath
    {
        libxml_use_internal_errors(true);

        $response = preg_replace('/(?=<!--)([\s\S]*?)-->/', '', trim($this->response));

        if (!empty($response)) {
            $doc = new DOMDocument();
            $doc->loadHTML((string)$response);

            return new DOMXPath($doc);
        }

        throw new \Exception('Empty response.');
    }

    public function getPath(): array
    {
        $parts = parse_url($this->url);

        return isset($parts['path']) ? explode('/', $parts['path']) : [];
    }

    public function isContentTypeOf(array $types): bool
    {
        return in_array($this->getContentType(), $types, true);
    }
}
