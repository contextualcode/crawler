<?php

declare(strict_types=1);

namespace ContextualCode\Crawler\Repository;

use ContextualCode\Crawler\Entity\Page;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class PageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Page::class);
    }

    public function store(Page $page): void
    {
        $em = $this->getEntityManager();

        $em->persist($page);
        $em->flush();
    }

    public function remove(string $identifier): void
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->delete(Page::class, 'p')
            ->where('p.identifier = :identifier')
            ->setParameter('identifier', $identifier);

        $qb->getQuery()->execute();
    }

    public function reset(string $identifier): void
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->update(Page::class, 'p')
            ->set('p.isProcessed', 0)
            ->where('p.identifier = :identifier')
            ->setParameter('identifier', $identifier);

        $qb->getQuery()->execute();
    }

    public function getRefererPage(Page $page): ?Page
    {
        $criteria = [
            'identifier' => $page->getIdentifier(),
            'url' => $page->getReferer(),
        ];

        return $this->findOneBy($criteria);
    }

    public function findBy(
        array $criteria,
        array $orderBy = null,
        $limit = null,
        $offset = null
    ): array {
        if (null === $orderBy) {
            $orderBy = ['pageOrder' => 'asc'];
        }

        return parent::findBy($criteria, $orderBy, $limit, $offset);
    }

    public function getMimeTypes(string $identifier): array
    {
        $qb = $this->getEntityManager()->createQueryBuilder()
            ->select('p.contentType, count(p.url) as count')
            ->from(Page::class, 'p')
            ->groupBy('p.contentType')
            ->orderBy('count', 'DESC')
            ->where('p.identifier = :identifier')
            ->setParameter('identifier', $identifier);

        return $qb->getQuery()->execute();
    }
}
