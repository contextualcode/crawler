<?php

declare(strict_types=1);

namespace ContextualCode\Crawler\Service;

use ContextualCode\Crawler\Entity\Page;
use ContextualCode\Crawler\Exception\InvalidLink;
use ContextualCode\Crawler\Helper\Link;
use ContextualCode\Crawler\Repository\PageRepository;
use ContextualCode\Crawler\Service\Handler as CrawlerHandler;
use DOMElement;
use Exception;
use InvalidArgumentException;
use Monolog\Handler\FingersCrossedHandler;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Helper\ProgressBar;

class Crawler
{
    /** @var Messages */
    private $messages;

    /** @var LoggerInterface */
    private $logger;

    /** @var PageRepository */
    private $pageRepository;

    /** @var CrawlerHandler[] */
    private $handlers = [];

    /** @var CrawlerHandler */
    private $handler;

    /** @var ProgressBar */
    private $progressBar;

    public function __construct(
        Messages $messages,
        LoggerInterface $crawlerLogger,
        PageRepository $pageRepository,
        iterable $crawlerHandlers
    ) {
        $this->messages = $messages;
        $this->logger = $crawlerLogger;
        $this->pageRepository = $pageRepository;

        foreach ($crawlerHandlers as $handler) {
            if ($handler instanceof CrawlerHandler) {
                $this->handlers[$handler->getImportIdentifier()] = $handler;
            }
        }
    }

    public function setHandler(string $identifier): void
    {
        if (!count($this->handlers)) {
            throw new InvalidArgumentException($this->messages->get('error_no_available_handlers'));
        }

        if (!isset($this->handlers[$identifier])) {
            $message = $this->messages->get('error_invalid_handler', [$identifier, implode('", "', array_keys($this->handlers))]);

            throw new InvalidArgumentException($message);
        }

        $this->handler = $this->handlers[$identifier];
    }

    public function getHandler(): Handler
    {
        return $this->handler;
    }

    public function setProgressBar(ProgressBar $progressBar): void
    {
        $this->progressBar = $progressBar;
    }

    public function getLogFilepath(): string
    {
        $handler = $this->logger->getHandlers()[0];
        if ($handler instanceof FingersCrossedHandler) {
            $handler = $handler->getHandler();
        }

        return $handler->getUrl();
    }

    public function reset(bool $purge = true): void
    {
        $identifier = $this->handler->getImportIdentifier();

        if ($purge) {
            $this->pageRepository->remove($identifier);
        } else {
            $this->pageRepository->reset($identifier);
        }
    }

    public function getImportIdentifier(): string
    {
        return $this->handler->getImportIdentifier();
    }

    public function getHomePageUrl(): string
    {
        return $this->getPageAbsoluteUrl($this->handler->getHomepage());
    }

    public function run(): array
    {
        $homepage = $this->getPage($this->getHomePageUrl());
        $this->getLinkedPages($homepage);

        foreach($this->handler->getExtraPages() as $extraPage) {
            $this->getPage($this->getPageAbsoluteUrl($extraPage));
        }

        $criteria = [
            'identifier' => $this->handler->getImportIdentifier(),
            'isProcessed' => false,
        ];
        while ($this->countPages(null, false)) {
            $page = $this->pageRepository->findOneBy($criteria);
            if (!$page instanceof Page) {
                break;
            }

            $this->getLinkedPages($page);
        }

        $this->setOrders();

        return [
            'valid' => $this->countPages(true),
            'invalid' => $this->countPages(false),
        ];
    }

    private function getPageAbsoluteUrl(string $page): string
    {
        return $this->handler->getProtocol() . '://' . $this->handler->getDomain() . $page;
    }

    private function getPage(string $url): Page
    {
        $identifier = $this->handler->getImportIdentifier();
        $criteria = ['identifier' => $identifier, 'url' => $url];
        $page = $this->pageRepository->findOneBy($criteria);

        if (null === $page) {
            $page = new Page($identifier, $url);
            $this->pageRepository->store($page);
        }

        return $page;
    }

    private function countPages(?bool $valid = null, ?bool $processed = null): int
    {
        $criteria = ['identifier' => $this->handler->getImportIdentifier()];

        if (null !== $valid) {
            $criteria['isValid'] = $valid;
        }

        if (null !== $processed) {
            $criteria['isProcessed'] = $processed;
        }

        return $this->pageRepository->count($criteria);
    }

    private function getLinkedPages(Page $page): array
    {
        $linkedPages = [];
        if ($page->isProcessed()) {
            return $linkedPages;
        }

        $this->updateProgressBar($page);

        try {
            $this->fetchPage($page);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());

            return $linkedPages;
        }

        if (!$page->isContentTypeOf($this->handler->getPageWithLinksContentTypes())) {
            return $linkedPages;
        }

        if ($page->isValid()) {
            $linkedPages = $page->getContentType() === 'text/css' ? $this->extractCssLinkedPages($page) : $this->extractLinkedPages($page);
        }

        return $linkedPages;
    }

    private function updateProgressBar(Page $page): void
    {
        if ($this->progressBar instanceof ProgressBar) {
            $this->progressBar->setMessage(trim((string) $page->getUrl()), 'url');
            $this->progressBar->setMessage(str_pad(trim((string) $page->getReferer()), 80), 'referer');
            $this->progressBar->setMaxSteps(max(1, $this->countPages()));
            $this->progressBar->setProgress($this->countPages(null, true));
        }
    }

    private function extractLinkedPages(Page $page): array
    {
        $context = [
            'url' => $page->getUrl(),
            'referer' => $page->getReferer(),
        ];

        $pages = [];
        try {
            $xPath = $page->getResponseXPath();
        } catch (Exception $e) {
            $this->logger->error($this->messages->get('unable_get_xpath'), $context);

            return $pages;
        }

        foreach ($this->handler->getPageLinksXpathSelectors() as $selector) {
            $links = $xPath->query($selector);

            $context['selector'] = $selector;
            $this->logger->debug($this->messages->get('pages_extracted', [$links->length]), $context);

            $counter = 0;
            foreach ($links as $link) {
                $href = (string) $this->extractLinkUrl($link);
                $context = [
                    'link_path' => $link->getNodePath(),
                    'page_url' => $page->getUrl(),
                    'page_referer' => $page->getReferer(),
                    'xpath_selector' => $selector,
                ];

                $linkedPage = $this->getLinkedPageByHref($page, $href, $pages, $context);
                if ($linkedPage === null) {
                    continue;
                }

                $linkedPage->setRefererPath($link->getNodePath());
                $this->pageRepository->store($linkedPage);

                $pages[$linkedPage->getUrl()] = $linkedPage;

                ++$counter;
            }

            $this->logger->debug($this->messages->get('pages_added', [$counter]), $context);
        }

        $counter = 0;
        $context = [
            'page_url' => $page->getUrl(),
            'page_referer' => $page->getReferer(),
        ];
        foreach ($this->handler->extractAdditionalURLs($xPath) as $extraUrl) {
            $linkedPage = $this->getLinkedPageByHref($page, $extraUrl, $pages, $context);
            if ($linkedPage === null) {
                continue;
            }

            $this->pageRepository->store($linkedPage);

            $pages[$linkedPage->getUrl()] = $linkedPage;

            ++$counter;
        }

        $this->logger->debug($this->messages->get('additional_pages_added', [$counter]), $context);

        return $pages;
    }

    private function getLinkedPageByHref(
        Page $page,
        string $href,
        array $pages,
        array $context = []
    ): ?Page {
        $url = $this->getUrlFromLink($page, $href, $context);

        if (null === $url || isset($pages[$url])) {
            return null;
        }

        $linkedPage = $this->getPage($url);
        if ($linkedPage->isProcessed()) {
            return null;
        }

        $linkedPage->setReferer($page->getUrl());

        return $linkedPage;
    }

    private function extractCssLinkedPages(Page $page): array
    {
        $context = [
            'url' => $page->getUrl(),
            'referer' => $page->getReferer(),
        ];

        $pages = [];
        preg_match_all('|url\((?![\'"]?(?:data):)[\'"]?([^\'"\)]*)[\'"]?\)|', $page->getResponse(), $urls);

        $counter = 0;
        foreach ($urls[1] as $href) {
            $url = $this->getUrlFromLink($page, $href);
            if (null === $url || isset($pages[$url])) {
                continue;
            }

            $linkedPage = $this->getPage($url);
            if ($linkedPage->isProcessed()) {
                continue;
            }

            $linkedPage->setReferer($page->getUrl());
            $this->pageRepository->store($linkedPage);

            $pages[$linkedPage->getUrl()] = $linkedPage;

            ++$counter;
        }

        $this->logger->debug($this->messages->get('pages_added', [$counter]), $context);

        return $pages;
    }

    private function getUrlFromLink(
        Page $page,
        string $href,
        array $context = []
    ): ?string {
        $referer = $page->getRedirectUrl() ?: $page->getUrl();
        $link = new Link($href, $referer);

        if (0 === count($context)) {
            $context = [
                'page_url' => $page->getUrl(),
                'page_referer' => $page->getReferer(),
            ];
        }

        try {
            $link->validate(
                $this->getWhitelistedDomains(),
                $this->getWhitelistedProtocols()
            );
        } catch (InvalidLink $e) {
            $message = $this->messages->get('link_skipped', [$e->getMessage()]);
            $context['link_url'] = $link->getUrl();
            $this->logger->debug($message, $context);

            return null;
        }

        $link->fixUrl($this->handler->getProtocol(), $this->handler->getDomain());

        try {
            $link->validateExpressions(
                $this->handler->getPageLinksIncludeRegularExpressions(),
                $this->handler->getPageLinksSkipRegularExpressions()
            );
        } catch (InvalidLink $e) {
            $message = $this->messages->get('link_skipped', [$e->getMessage()]);
            $context['link_url'] = $link->getUrl();
            $this->logger->debug($message, $context);

            return null;
        }

        return $this->handler->fixUrl($link->getUrl());
    }

    private function extractLinkUrl(DOMElement $element): ?string
    {
        $attributes = $this->handler->getPageLinksUrlAttributes();
        $attributes = $attributes[$element->tagName] ?? [];
        if (count($attributes) === 0) {
            $message = $this->messages->get('unable_extract_url', [$element->tagName, $element->getNodePath()]);
            $this->logger->debug($message);

            return null;
        }

        foreach ($attributes as $attribute) {
            if ($element->hasAttribute($attribute)) {
                $value = $element->getAttribute($attribute);
                if ($value) {
                    return $this->handler->fixUrl($value);
                }
            }
        }

        return null;
    }

    private function getWhitelistedDomains(): array
    {
        return array_unique(array_merge(
            [$this->handler->getDomain()],
            $this->handler->getWhitelistedDomains()
        ));
    }

    private function getWhitelistedProtocols(): array
    {
        return array_unique(array_merge(
            [$this->handler->getProtocol()],
            $this->handler->getWhitelistedProtocols()
        ));
    }

    private function fetchPage(Page $page): void
    {
        $response = $this->requestUrl($page->getUrl(), $page->getReferer());

        $error = $response['error_message'];
        if (null === $error && $response['http_status'] !== 200) {
            $error = 'HTTP status code is ' . $response['http_status'];
        }

        $page->setContentType((string) $response['content_type']);
        $page->setStatusCode((int) $response['http_status']);
        $page->setErrorMessage($error);
        $page->setHeaders($response['headers']);
        $page->setValid(null === $error);
        $page->setProcessed(true);
        if ($page->isContentTypeOf($this->handler->getPageContentTypesToStoreResponse())) {
            $page->setResponse($response['response']);
        }
        $nonRedirectFinalUrls = [$page->getUrl(), Link::encodeUrl($page->getUrl())];
        if ($response['final_url'] && !in_array($response['final_url'], $nonRedirectFinalUrls, true)) {
            $page->setRedirectUrl($response['final_url']);
        }
        $this->pageRepository->store($page);
    }

    private function requestUrl(string $url, ?string $referer = null): array
    {
        $info = [
            'response' => null,
            'headers' => [],
            'content_type' => null,
            'error_message' => null,
            'http_status' => null,
            'final_url' => null,
        ];

        $curl = curl_init(Link::encodeUrl($url));
        curl_setopt($curl, CURLOPT_USERAGENT, $this->handler->getUserAgent());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, $this->handler->followRedirects());
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, $this->handler->getConnectionTimeout());
        curl_setopt($curl, CURLOPT_TIMEOUT, $this->handler->getRequestTimeout());
        if ($referer) {
            curl_setopt($curl, CURLOPT_REFERER, $referer);
        }
        if (!$this->handler->verifySsl()) {
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        }
        $this->handler->customizeRequest($curl);

        $headers = [];
        curl_setopt($curl, CURLOPT_HEADERFUNCTION,
            static function ($resource, $header) use (&$headers) {
                $parts = explode(':', $header);
                if (count($parts) > 1) {
                    $name = strtolower(trim($parts[0]));
                    $value = trim(implode(':', array_slice($parts, 1)));

                    $headers[$name][] = $value;
                }

                return strlen($header);
            }
        );

        $response = curl_exec($curl);
        if ($response === false) {
            $info['error_message'] = curl_error($curl);
        } else {
            $info['response'] = $response;
            $info['headers'] = $headers;
            $info['content_type'] = curl_getinfo($curl, CURLINFO_CONTENT_TYPE);
            $info['http_status'] = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $info['final_url'] = curl_getinfo($curl, CURLINFO_EFFECTIVE_URL);
        }

        curl_close($curl);

        $context = [
            'url' => $url,
            'referer' => $referer,
            'content_type' => $info['content_type'],
            'error_code' => $info['error_message'],
            'http_status' => $info['http_status'],
            'final_url' => $info['final_url'],
        ];

        $this->logger->debug($this->messages->get('request_sent'), $context);

        $delay = $this->handler->delayBetweenRequests();
        if ($delay > 0) {
            sleep($delay);
        }

        return $info;
    }

    private function setOrders(): void
    {
        $pages = $this->pageRepository->findBy(['identifier' => $this->handler->getImportIdentifier()]);

        uasort($pages, static function ($a, $b) {
            $pathCountA = count($a->getPath());
            $pathCountB = count($b->getPath());
            
            if ($pathCountA === $pathCountB) {
                $aIsIndex = (strpos(implode('/', $a->getPath()), 'index') !== false);
                $bIsIndex = (strpos(implode('/', $b->getPath()), 'index') !== false);

                if ($aIsIndex === $bIsIndex) {
                    return strcmp($a->getUrl(), $b->getUrl());
                }

                return $aIsIndex ? -1 : 1;
            }

            return $pathCountA > $pathCountB ? 1 : -1;
        });

        $order = 1;
        foreach ($pages as $page) {
            $page->setPageOrder($order);
            $this->pageRepository->store($page);
            ++$order;
        }
    }
}
