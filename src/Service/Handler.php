<?php

declare(strict_types=1);

namespace ContextualCode\Crawler\Service;

use DOMXPath;

abstract class Handler
{
    public const USER_AGENT = 'Contextual Code / Import Crawler';

    abstract public function getImportIdentifier(): string;

    abstract public function getDomain(): string;

    public function getProtocol(): string
    {
        return 'https';
    }

    public function verifySsl(): bool
    {
        return true;
    }

    public function getHomepage(): string
    {
        return '/';
    }

    public function getExtraPages(): array
    {
        return [];
    }

    public function getUserAgent(): string
    {
        return self::USER_AGENT;
    }

    public function followRedirects(): bool
    {
        return true;
    }

    public function getConnectionTimeout(): int
    {
        return 20;
    }

    public function getRequestTimeout(): int
    {
        return 60;
    }

    public function delayBetweenRequests(): int
    {
        return 0;
    }

    public function getPageContentTypesToStoreResponse(): array
    {
        return [
            'text/html',
            'application/xhtml+xml',
        ];
    }

    public function getPageWithLinksContentTypes(): array
    {
        // https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types
        return [
            'text/html',
            'application/xhtml+xml',
        ];
    }

    public function getPageLinksXpathSelectors(): array
    {
        return [
            // All links
            '//a[@href]',
            // All images
            '//img[@src]',
        ];
    }

    public function getPageLinksUrlAttributes(): array
    {
        return [
            'a' => ['href'],
            'img' => ['src'],
        ];
    }

    public function getPageLinksIncludeRegularExpressions(): array
    {
        return [];
    }

    public function getPageLinksSkipRegularExpressions(): array
    {
        return [];
    }

    public function getWhitelistedDomains(): array
    {
        return [];
    }

    public function getWhitelistedProtocols(): array
    {
        return [];
    }

    public function fixUrl(string $url): string
    {
        return $url;
    }

    public function customizeRequest($curl): void
    {
    }

    public function extractAdditionalURLs(DOMXPath $response): array
    {
        return [];
    }
}
